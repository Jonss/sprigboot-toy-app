# Springboot toy app

# Pré-requisitos:
- docker
- docker-compose
- java 8
- Make (se não me engano é padrão no linux)

# Como executar?
- make run

Ou

mvn install -DskipTests
docker-compose up --build