FROM openjdk:8-jdk-alpine

RUN mkdir /yeap

COPY target/yeap-0.0.1-SNAPSHOT.jar /yeap/app.jar

WORKDIR /yeap

CMD ["sh", "-c", "java -Dspring.profiles.active=$JAVA_ENV -Djava.security.egd=file:/dev/./urandom -Xms128m -Xmx512m -jar app.jar"]
