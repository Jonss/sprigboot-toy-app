package br.com.jupiterstein.yeap.repository;

import br.com.jupiterstein.yeap.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuaMaeRepository extends JpaRepository<User, Long> {
}
