package br.com.jupiterstein.yeap.repository;

import br.com.jupiterstein.yeap.model.Book;
import br.com.jupiterstein.yeap.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findAllByUser(User user);

    List<Book> findAllByCreatedAtBetween(LocalDateTime start, LocalDateTime end);

    Optional<Book> findByTitleAndUserId(String title, Long id);

    Optional<Book> findByTitleAndUserEmail(String title, String email);
}
