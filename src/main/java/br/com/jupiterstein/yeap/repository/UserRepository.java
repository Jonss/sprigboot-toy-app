package br.com.jupiterstein.yeap.repository;

import br.com.jupiterstein.yeap.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    //@Query("User u where u.createdAt >= $1 and u.createdAt <= $2") //JPQL
    //@Query(value = "select * from users where created_at between $1 and $2", nativeQuery = true) // Nativo
    //List<User> findAae(LocalDateTime start, LocalDateTime end);

}