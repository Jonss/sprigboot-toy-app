package br.com.jupiterstein.yeap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@EnableJpaRepositories
public class YeapApplication {

	@GetMapping
	public String hello() {
		return "Hello yeap";
	}

	public static void main(String[] args) {
		SpringApplication.run(YeapApplication.class, args);
	}

}
