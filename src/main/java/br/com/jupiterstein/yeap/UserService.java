package br.com.jupiterstein.yeap;

import br.com.jupiterstein.yeap.model.User;
import br.com.jupiterstein.yeap.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User create(User user) {
        return userRepository.save(user);
    }
}
