package br.com.jupiterstein.yeap.repository;

import br.com.jupiterstein.yeap.model.Book;
import br.com.jupiterstein.yeap.model.User;
import br.com.jupiterstein.yeap.repository.base.RepositoryTestBase;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class BookRepositoryTest extends RepositoryTestBase {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void shouldFindBooksByUser() {
        // Cenário
        User user = userRepository.save(new User("jupiter.stein@gmail.com"));

        Book umaOdisseiaNoEspaco = new Book("Ficção científica", "2001: Uma Odisséia no espaço", user);
        Book oHobbit = new Book("Fantasia", "O Hobbit", user);

        List<Book> books = Arrays.asList(umaOdisseiaNoEspaco, oHobbit);
        books.forEach(bookRepository::save);

        // Ação
        List<Book> booksByUser = bookRepository.findAllByUser(user);

        // Validação
        Assertions.assertNotNull(booksByUser);
        Assertions.assertEquals(2, booksByUser.size());

        Book firstBook = booksByUser.get(0);
        Book secondBook = booksByUser.get(1);

         Assertions.assertNotNull(firstBook.getId());
        Assertions.assertEquals("Ficção científica", firstBook.getSubject());
        Assertions.assertEquals("2001: Uma Odisséia no espaço", firstBook.getTitle());
        Assertions.assertEquals(user, firstBook.getUser());

        Assertions.assertNotNull(secondBook.getId());
        Assertions.assertEquals("Fantasia", secondBook.getSubject());
        Assertions.assertEquals("O Hobbit", secondBook.getTitle());
        Assertions.assertEquals(user, secondBook.getUser());
    }

    @Test
    public void shouldFindBooksByTest() {
        // Cenário
        User user = userRepository.save(new User("jupiter.stein@gmail.com"));

        Book umaOdisseiaNoEspaco = new Book("Ficção científica",
                "2001: Uma Odisséia no espaço",
                user,
                LocalDateTime.of(LocalDate.parse("1968-05-25"), LocalTime.NOON));

        Book oHobbit = new Book("Fantasia", "O Hobbit", user,
                LocalDateTime.of(LocalDate.parse("1952-05-25"), LocalTime.NOON));

        Book harryPotter = new Book("Fantasia", "Harry Potter e o prisioneiro de Azkaban", user,
                LocalDateTime.of(LocalDate.parse("2000-05-25"), LocalTime.NOON));

        List<Book> books = Arrays.asList(umaOdisseiaNoEspaco, oHobbit,harryPotter);
        books.forEach(b -> bookRepository.save(b));

        LocalDateTime start = LocalDateTime.of(LocalDate.parse("1960-01-31"), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.parse("2001-01-31"), LocalTime.of(23, 59));

        // Ação
        List<Book> booksFound = bookRepository.findAllByCreatedAtBetween(start, end);

        Assertions.assertNotNull(booksFound);
        Assertions.assertEquals(2, booksFound.size());
        Assertions.assertEquals("2001: Uma Odisséia no espaço", booksFound.get(0).getTitle());
        Assertions.assertEquals("Harry Potter e o prisioneiro de Azkaban", booksFound.get(1).getTitle());
    }

    @Test
    public void shouldFindBookByTitleAndUserId() {
        User user = userRepository.save(new User("jupiter.stein3@gmail.com"));

        Book oHobbit = new Book("Fantasia", "O Hobbit", user,
                LocalDateTime.of(LocalDate.parse("1952-05-25"), LocalTime.NOON));

        bookRepository.save(oHobbit);

        Optional<Book> opBook = bookRepository.findByTitleAndUserId("O Hobbit", user.getId());
        Optional<Book> opBook2 = bookRepository.findByTitleAndUserEmail("O Hobbit", "jupiter.stein3@gmail.com");

        // findByTitleAndUserAddressStreet("title", "rua x");

        Assertions.assertTrue(opBook.isPresent());
        Assertions.assertEquals("O Hobbit", opBook.get().getTitle());

        Assertions.assertTrue(opBook2.isPresent());
        Assertions.assertEquals("O Hobbit", opBook2.get().getTitle());
    }

    /*
    * select book0_.id as id1_0_, book0_.created_at as created_2_0_,
    *  book0_.subject as subject3_0_, book0_.title as title4_0_,
    * book0_.user_id as user_id5_0_ from books
    * book0_ left outer join users user1_
    * on book0_.user_id=user1_.id
    * where book0_.title=? and user1_.id=?
    * */

    /*
    * select book0_.id as id1_0_, book0_.created_at as created_2_0_, book0_.subject as subject3_0_, book0_.title as title4_0_, book0_.user_id as user_id5_0_ from books book0_ left outer join users user1_ on book0_.user_id=user1_.id where book0_.title=? and user1_.email=?
    * */
}
