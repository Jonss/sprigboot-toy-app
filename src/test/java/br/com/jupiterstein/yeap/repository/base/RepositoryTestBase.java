package br.com.jupiterstein.yeap.repository.base;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.sql.DataSource;

@DataJpaTest
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
public class RepositoryTestBase {

    @Autowired
    private DataSource dataSource;

}
