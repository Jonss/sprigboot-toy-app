package br.com.jupiterstein.yeap.repository;

import br.com.jupiterstein.yeap.model.User;
import br.com.jupiterstein.yeap.repository.base.RepositoryTestBase;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@WebAppConfiguration
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserRepositoryTest extends RepositoryTestBase {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void shouldPersistUser() {
        // Cenário
        User user = new User("jupiter.stein@gmail.com");

        // Ação
        User savedUser = userRepository.save(user);

        // Validação
        Assertions.assertNotNull(savedUser);
        Assertions.assertEquals("jupiter.stein@gmail.com", savedUser.getEmail());
    }

    @Test
    public void shouldFindUserByEmail() {
        // Cenário
        User user = new User("jupiter.stein@gmail.com");
        userRepository.save(user);

        // Ação
        User userByEmail = userRepository.findByEmail("jupiter.stein@gmail.com");

        // Validação
        Assertions.assertNotNull(userByEmail);
        Assertions.assertEquals("jupiter.stein@gmail.com", userByEmail.getEmail());
        Assertions.assertNotNull(userByEmail.getId());
    }

    @Test
    public void shouldNotFindUserNotSaved() {
        // Ação/Cenário
        User userByEmail = userRepository.findByEmail("jupiter.stein@gmail.com");

        // Validação
        Assertions.assertNull(userByEmail);
    }

}